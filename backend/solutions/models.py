# backend/solutions/models.py

from django.db import models
from model_utils.fields import StatusField
from model_utils import Choices


class Solution(models.Model):
    """Model of solution"""

    STATUS = Choices('evaluation', 'correct', 'wrong')

    code = models.TextField()
    date = models.DateTimeField('date submitted', auto_now_add=True)
    status = StatusField()
