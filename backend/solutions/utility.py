# backend/solutions/utility.py

from .models import Solution


def check_status(status):
    if status == Solution.STATUS.evaluation or status == Solution.STATUS.correct or status == Solution.STATUS.wrong:
        return True
    else:
        return False
