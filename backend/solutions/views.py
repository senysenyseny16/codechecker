# backend/solutions/views.py

import logging
import json
from rest_framework.views import APIView
from rest_framework.response import Response
from django.http import HttpResponse
from django.shortcuts import get_object_or_404

from .models import Solution
from .serializers import SolutionSerializer
from .tasks import verify_solution


class SolutionView(APIView):
    def post(self, request):
        """Handles post requests to verify solution in request.data"""

        solution_serialized = SolutionSerializer(data=request.data)
        if solution_serialized.is_valid():
            solution = solution_serialized.save()

            # Send solution to verifying third-party service.
            verify_solution.delay(solution.id)

            # Send solution id and status in response.
            return Response({'id': solution.id, 'status': solution.status})
        else:
            return Response(solution_serialized.errors)

    def get(self, request):
        """Returns all solutions"""
        solutions = Solution.objects.all()
        serializer = SolutionSerializer(solutions, many=True)
        return Response(serializer.data)


# I think the following two functions should belong to view, but don't know how-to do it.
def get_solution(request, id):
    """Returns solution by id"""
    solution = get_object_or_404(Solution, pk=id)
    # Don't know how-to serialize properly :( and field date is not serializable.
    response = {'code': solution.code, 'id': solution.id, 'status': solution.status}
    return HttpResponse(json.dumps(response))


def get_status(request, id):
    """Returns solutions status by id"""
    solution = get_object_or_404(Solution, pk=id)
    response = {'id': solution.id, 'status': solution.status}
    return HttpResponse(json.dumps(response))
