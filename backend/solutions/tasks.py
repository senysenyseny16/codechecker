# backend/solutions/tasks

import logging
from celery import task

from .models import Solution
from .thirdparty import Client
from .utility import check_status

retry_interval = 10  # seconds
check_interval = 1   # seconds

# I will use the shorthand: thirdparty -> tp.


@task
def verify_solution(solution_id: int):
    """Verifies solution on third-party service

    Parameters
    ----------
    solution_id : int
        Id of solution that we should to verify.

    """

    try:
        solution = Solution.objects.get(pk=solution_id)
        tp_task_id, status = Client.post_submission(solution.code)

        # There are two options:
        # 1. Third-party service did not have time to evaluate the correctness of the solution:
        #    status == evaluation.
        #    In this case we should save third-party service task id for our solution and periodically check
        #    the status of that task.
        # 2. Third-party service has already evaluated the correctness, then we have to update the status and exit.
        if status == Solution.STATUS.evaluation:
            check_solution_status.apply_async((solution_id, tp_task_id), countdown=check_interval)
        else:
            if check_status(status):
                solution.status = status
                solution.save()
            else:
                # I think in this case we can retry verifying.
                logging.warning('Third-party service returns bad status for solution with id: %s', solution_id)
                verify_solution.delay(solution_id)

    except Solution.DoesNotExist:
        # We lost the solution. In this case we should do something special.
        logging.error('Solution with id: %s not found', id)

    except BaseException:
        verify_solution.apply_async((solution_id, ), countdown=retry_interval)


@task
def check_solution_status(solution_id: int, tp_task_id: int):
    """Checks evaluation status of solution on third-party service.

    Parameters
    ----------
    solution_id: int
        Id of solution, that third-party service verifying.

    tp_task_id: int
        Id of third-party service task, that verifying solution with solution_id.

    """

    try:
        # Update id and status from third-party service.
        new_tp_task_id, new_status = Client.get_submission(tp_task_id)

        if new_status == Solution.STATUS.evaluation:
            # Schedule status update with new third-party task id.
            check_solution_status.apply_async((solution_id, new_tp_task_id), countdown=check_interval)
        else:
            if check_status(new_status):
                # Verification completed, update status and exit.
                solution = Solution.objects.get(pk=solution_id)
                solution.status = new_status
                solution.save()
            else:
                # I think in this case we can retry verifying.
                logging.warning('Third-party service returns bad status for solution with id: %s', solution_id)
                verify_solution.delay(solution_id)

    except Solution.DoesNotExist:
        # We lost the solution. In this case we should do something special.
        logging.error('Solution with id: %s not found', id)

    except BaseException:
        logging.warning('Something is wrong, trying to update solution status with id: %s', id)
        check_solution_status.apply_async((id,), countdown=retry_interval)
