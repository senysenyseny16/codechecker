# backend/solutions/urls.py

from django.urls import path, re_path
from .views import SolutionView, get_solution, get_status


urlpatterns = [
    path('solutions/', SolutionView.as_view()),  # I think it could be separate to two paths: submit and get_solutions,
                                                 # it would be clearer.
    path('solution/<int:id>', get_solution, name='get_solution'),
    path('status/<int:id>', get_status, name='get_status'),
]
