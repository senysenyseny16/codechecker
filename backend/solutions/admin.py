from django.contrib import admin
from rangefilter.filter import DateRangeFilter, DateTimeRangeFilter
from .models import Solution


class SolutionAdmin(admin.ModelAdmin):
    readonly_fields = ('id', 'status', 'code', 'date')
    list_display = ('id', 'status', 'code', 'date')
    list_filter = (
        'status', ('date', DateTimeRangeFilter),
    )

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj=None):
        return False

    # Disable save/delete functionality.
    def change_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['readonly'] = True
        extra_context['show_save_and_add_another'] = False
        extra_context['show_save_and_continue'] = False
        extra_context['show_save'] = False

        return super(SolutionAdmin, self).change_view(request, object_id, extra_context=extra_context)


admin.site.register(Solution, SolutionAdmin)
