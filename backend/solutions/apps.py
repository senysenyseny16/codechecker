from django.apps import AppConfig


class SolutionsConfig(AppConfig):
    name = 'solutions'

    def ready(self):
        # Some solutions may not be evaluated at the moment of server shutdown.
        # Here we can verify them.
        from .models import Solution
        from .tasks import verify_solution

        evaluating_solutions = Solution.objects.filter(status=Solution.STATUS.evaluation)
        for solution in evaluating_solutions:
            verify_solution(solution.id)
