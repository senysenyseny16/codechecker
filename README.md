:v:

## Check code

There is a problem to be solved (for example: calculate sum of two numbers).\
Users can write some solutions, submit them, check the correctness of the solutions.

The project consists of two folders:

- backend: based on Django and Celery (configured for Redis).
- frontend: based on Vue.js framework.

## Backend

The main model of codecheker is `Solution` `(backend/solutions/models.py)`.
This model represents the user solution of the problem: computer code, evaluation status and submission date.

`SolutionView` (`backend/solutions/views.py`) is used to connect `Solution` with third-party verification service and frontend (API):
- `post` to submit the solution and `get` to get all solutions.
- `get_solution` to get the solution with `id`.
- `get_status` to get the status of the solution with `id`.

These functions are mapped to urls in ```backend/solutions/urls.py```.

When a user submits the solution we immediately create Celery task for the solution verification (`backend/solutions/tasks::verify_solution`), that task periodically checks the status of the solution on the third-party service (`backend/solutions/tasks::check_solution_status`).

To run backend, enter to the `backend` folder and execute the following commands:
```
python manage.py runserver
celery worker -A codechecker --loglevel=debug --concurrency=4
```

(install the required packages before, `requirements.txt`)

## Frontend

The frontend consists of one simple page (component): `frontend/src/components/Home.vue`.

In this page the user can enter the solution in code editor, submit it and check the correctness of this solution.

When submit button is pressed, frontend sends the solution code to backend (`submitSolution()`), locks page and starts periodically checking the evaluation status (`checkSolution()`).

Periodical checking is pretty straightforward, every second frontend sends `get` request for solution status.\
To start frontend, enter to the `frontend` folder and execute the following command:
```
npm run dev
```

## Django administration page

You can view and filter all submitted solutions in standard Django administration page `(http://localhost/admin)`, I created administrator account for you:

login: `baby`\
password: `sweet16`\
e-mail address: sweet@baby.com
